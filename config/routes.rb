Rails.application.routes.draw do
  
  get "new" => "tweets#new"
  post "tweets/create" => "tweets#create"
  get "tweets/:id" => "tweets#show"
  get "tweets/:id/destroy" => "tweets#destroy"
  get "tweets/:id/edit" => "tweets#edit"
  post "tweets/:id/update" => "tweets#update"
  get "tweets/:id/show" => "tweets#show"
  get '' => "tweets#index"
end
