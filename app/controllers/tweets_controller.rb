class TweetsController < ApplicationController
  def index
    @tweets=Tweet.all.order(created_at: :desc)
  end
  
  def new
  end
  
  def show
    @tweet = Tweet.find_by(id: params[:id])
  end
  
  def create
    @tweet=Tweet.new(message: params[:message])
    @tweet.tdate=Time.current
    if @tweet.save
      redirect_to("/")
    else
      render action: :new
    end
  end
  
  def destroy
    @tweet=Tweet.find_by(id: params[:id])
    @tweet.destroy
    redirect_to("/")
  end
  
  def edit
    @tweet=Tweet.find_by(id: params[:id])
  end
  
  def update
    @tweet = Tweet.find_by(id: params[:id])
    @tweet.message = params[:message]
    @tweet.save
    redirect_to("/")
  end
  
end
